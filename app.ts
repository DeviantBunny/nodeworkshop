import * as http from 'http'
import * as fs from 'fs'
import {MyFunction, myString} from "./testModule";

function OnRequest(request, response){
    response.writeHead(200, {'Content-Type': 'text/html'});
    fs.readFile('./index.html', null, function (error, data) {
        if (error) {
            response.writeHead(404);
            response.write('Close, but no cigar');
        } else {
            response.write(data);
        }
        response.end();
    });
}

http.createServer(OnRequest).listen(8451);