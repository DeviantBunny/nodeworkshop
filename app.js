"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var http = require("http");
var fs = require("fs");
function OnRequest(request, response) {
    response.writeHead(200, { 'Content-Type': 'text/html' });
    fs.readFile('./index.html', null, function (error, data) {
        if (error) {
            response.writeHead(404);
            response.write('Close, but no cigar');
        }
        else {
            response.write(data);
        }
        response.end();
    });
}
http.createServer(OnRequest).listen(8451);
