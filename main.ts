import * as url from 'url'
import * as fs from 'fs'

function renderHTML(path, response){
    fs.readFile(path, null, function (error, data) {
        if (error) {
            response.writeHead(404);
            response.write('Close, but no cigar');
        }
        else {
            response.write(data);
        }
        response.end();
    });
}

export function handleRequest (request, response){
    response.writeHead(200, {'Content-Type': 'text/html'});

    let path = url.parse(request.url).pathname;
    switch (path){
        case '/':
            renderHTML('./index.html', response);
            break;
        case '/login':
            renderHTML('./login.html', response);
            break;
        default:
            response.writeHead(404);
            response.write('Route 666 (Not Found)');
            response.end();
    }
}