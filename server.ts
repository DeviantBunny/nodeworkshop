import * as http from 'http'
import * as main from './main'

http.createServer(main.handleRequest).listen(8000);