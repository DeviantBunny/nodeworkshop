"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.handleRequest = void 0;
var url = require("url");
var fs = require("fs");
function renderHTML(path, response) {
    fs.readFile(path, null, function (error, data) {
        if (error) {
            response.writeHead(404);
            response.write('Close, but no cigar');
        }
        else {
            response.write(data);
        }
        response.end();
    });
}
function handleRequest(request, response) {
    response.writeHead(200, { 'Content-Type': 'text/html' });
    var path = url.parse(request.url).pathname;
    switch (path) {
        case '/':
            renderHTML('./index.html', response);
            break;
        case '/login':
            renderHTML('./login.html', response);
            break;
        default:
            response.writeHead(404);
            response.write('Route 666 (Not Found)');
            response.end();
    }
}
exports.handleRequest = handleRequest;
